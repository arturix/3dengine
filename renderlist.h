#pragma once

#include "videobuff.h"
#include "camera.h"
#include "poly.h"
#include "objectcontainer.h"
#include "paint.h"

typedef struct  
{
	VIDEOBUFF		*vb;
	CAMERA			*cam;

#define RENDERLIST_MAX_POLYS	1026
	int32	numPolys;
	POLYR	plist[RENDERLIST_MAX_POLYS];
} RENDERLIST;

/************************************************************************/
/* renderlist_Create	                                                */
/************************************************************************/
int32 renderlist_Create( RENDERLIST *rl, VIDEOBUFF *vb, CAMERA *cam )
{
	rl->vb	= vb;
	rl->cam	= cam;
	rl->numPolys = 0;
	return 1;
}

/************************************************************************/
/* renderlist_Destroy                                                   */
/************************************************************************/
void renderlist_Destroy( RENDERLIST *rl )
{

}

/************************************************************************/
/* renderlist_Reset                                                     */
/************************************************************************/
INLINE void renderlist_Reset( RENDERLIST *rl )
{
	rl->numPolys	= 0;
}

/************************************************************************/
/* renderlist_AddObject                                                 */
/************************************************************************/
INLINE int32 renderlist_AddObject( RENDERLIST *rl, OBJECTCONTAINER *oc )
{

	int32 i, j;
	int32 pidx;
	POLY *p;
	POLYR *pr, *pr2;
	VERTEX *vert0, *vert1, *vert2;
	VERTEX *_vert0, *_vert1, *_vert2;
	VECTOR3D spherePos;
	real zTest;
	int32 cc0, cc1, cc2;
	int32 vertsIn;
	int32 vv0, vv1, vv2;
	VECTOR3D u, v, n;
	real t0, x0, y0;
	real t1, x1, y1;

	// Camera cull
	spherePos = matrix4x3_MulVec3D( oc->pos, rl->cam->mcam );
	if ( spherePos.z + oc->maxRadius < rl->cam->nearZ || spherePos.z - oc->maxRadius > rl->cam->farZ )
		return 0;

	zTest = spherePos.z * rl->cam->zFactorX;
	if ( spherePos.x + oc->maxRadius < -zTest || spherePos.x - oc->maxRadius > zTest )
		return 0;

	zTest = spherePos.z * rl->cam->zFactorY;
	if ( spherePos.y + oc->maxRadius < -zTest || spherePos.y - oc->maxRadius > zTest )
		return 0;

	for ( pidx = 0; pidx < oc->o->numPolys; pidx++ )
	{
		p = &oc->o->plist[pidx];
		pr = &rl->plist[rl->numPolys];		

		_vert0 = &oc->o->vlist[p->vidx[0]];
		_vert1 = &oc->o->vlist[p->vidx[1]];
		_vert2 = &oc->o->vlist[p->vidx[2]];

		vert0 = &pr->vlist[0];
		vert1 = &pr->vlist[1];
		vert2 = &pr->vlist[2];


		// Model to world	
		vert0->v = matrix4x3_MulVec3D( _vert0->v, oc->m );
		vert1->v = matrix4x3_MulVec3D( _vert1->v, oc->m );
		vert2->v = matrix4x3_MulVec3D( _vert2->v, oc->m );

		if ( !(oc->attr & ATTR_2SIDED) )
		{
			u = vec3D_Sub( vert1->v, vert0->v );
			v = vec3D_Sub( vert2->v, vert0->v );
			n = vec3D_Cross( u, v );
			spherePos = vec3D_Sub( vert0->v, rl->cam->pos );
			zTest = vec3D_Dot( spherePos, n );
			if ( zTest > 0 )
				continue;
		}		

		// World to camera
		vert0->v = matrix4x3_MulVec3D( vert0->v, rl->cam->mcam );
		vert1->v = matrix4x3_MulVec3D( vert1->v, rl->cam->mcam );
		vert2->v = matrix4x3_MulVec3D( vert2->v, rl->cam->mcam );
				

		// CLIP POLY
#define CLIP_CODE_G	0x0001
#define CLIP_CODE_L	0x0002
#define CLIP_CODE_I	0x0004

		// Clip X
		zTest = vert0->z * rl->cam->zFactorX;
		if ( vert0->x > zTest )
			cc0 = CLIP_CODE_G;
		else if ( vert0->x < -zTest )
			cc0 = CLIP_CODE_L;
		else
			cc0 = CLIP_CODE_I;

		zTest = vert1->z * rl->cam->zFactorX;
		if ( vert1->x > zTest )
			cc1 = CLIP_CODE_G;
		else if ( vert1->x < -zTest )
			cc1 = CLIP_CODE_L;
		else
			cc1 = CLIP_CODE_I;

		zTest = vert2->z * rl->cam->zFactorX;
		if ( vert2->x > zTest )
			cc2 = CLIP_CODE_G;
		else if ( vert2->x < -zTest )
			cc2 = CLIP_CODE_L;
		else
			cc2 = CLIP_CODE_I;

		if ( cc0 == CLIP_CODE_G && cc1 == CLIP_CODE_G && cc2 == CLIP_CODE_G )
			continue;

		if ( cc0 == CLIP_CODE_L && cc1 == CLIP_CODE_L && cc2 == CLIP_CODE_L )
			continue;

		// Clip Y
		zTest = rl->cam->zFactorY * vert0->z;
		if ( vert0->y > zTest )
			cc0 = CLIP_CODE_G;
		else if ( vert0->y < -zTest )
			cc0 = CLIP_CODE_L;
		else
			cc0 = CLIP_CODE_I;

		zTest = rl->cam->zFactorY * vert1->z;
		if ( vert1->y > zTest )
			cc1 = CLIP_CODE_G;
		else if ( vert1->y < -zTest )
			cc1 = CLIP_CODE_L;
		else
			cc1 = CLIP_CODE_I;

		zTest = rl->cam->zFactorY * vert2->z;
		if ( vert2->y > zTest )
			cc2 = CLIP_CODE_G;
		else if ( vert2->y < -zTest )
			cc2 = CLIP_CODE_L;
		else
			cc2 = CLIP_CODE_I;

		if ( cc0 == CLIP_CODE_G && cc1 == CLIP_CODE_G && cc2 == CLIP_CODE_G )
			continue;

		if ( cc0 == CLIP_CODE_L && cc1 == CLIP_CODE_L && cc2 == CLIP_CODE_L )
			continue;

		// Clip z
		vertsIn = 0;
		if ( vert0->z > rl->cam->farZ )
			cc0 = CLIP_CODE_G;
		else if ( vert0->z < rl->cam->nearZ )
			cc0 = CLIP_CODE_L;
		else
		{
			vertsIn++;
			cc0 = CLIP_CODE_I;
		}

		if ( vert1->z > rl->cam->farZ )
			cc1 = CLIP_CODE_G;
		else if ( vert1->z < rl->cam->nearZ )
			cc1 = CLIP_CODE_L;
		else
		{
			vertsIn++;
			cc1 = CLIP_CODE_I;
		}

		if ( vert2->z > rl->cam->farZ )
			cc2 = CLIP_CODE_G;
		else if ( vert2->z < rl->cam->nearZ )
			cc2 = CLIP_CODE_L;
		else 
		{
			vertsIn++;
			cc2 = CLIP_CODE_I;
		}

		if ( cc0 == CLIP_CODE_G && cc1 == CLIP_CODE_G && cc2 == CLIP_CODE_G )
			continue;

		if ( cc0 == CLIP_CODE_L && cc1 == CLIP_CODE_L && cc2 == CLIP_CODE_L )
			continue;

		pr->color	= oc->color;
		pr->attr = oc->attr;
		

		if ( ( cc0 | cc1 | cc2 ) & CLIP_CODE_L )
		{
			if ( vertsIn == 1 )
			{
				if ( cc0 == CLIP_CODE_I )
				{	
					vv0 = 0; vv1 = 1; vv2 = 2; 
				}
				else if ( cc1 == CLIP_CODE_I )
				{
					vv0 = 1; vv1 = 2; vv2 = 0;
				}
				else
				{
					vv0 = 2; vv1 = 0; vv2 = 1;
				}
			

				vert0 = &pr->vlist[vv0];
				vert1 = &pr->vlist[vv1];
				vert2 = &pr->vlist[vv2];

				// clip edge v0->v1
				u = vec3D_Sub( vert1->v, vert0->v );
				t0 = ( rl->cam->nearZ - vert0->z ) / u.z;
				x0 = vert0->x + u.x * t0;
				y0 = vert0->y + u.y * t0;

				// clip edge v0->v2
				v = vec3D_Sub( vert2->v, vert0->v );
				t1 = ( rl->cam->nearZ - vert0->z ) / v.z;
				x1 = vert0->x + v.x * t1;
				y1 = vert0->y + v.y * t1;

				vert1->x = x0;
				vert1->y = y0;
				vert1->z = rl->cam->nearZ;

				vert2->x = x1;
				vert2->y = y1;
				vert2->z = rl->cam->nearZ;

			} // end if vertsIn = 1
			else if ( vertsIn == 2 )
			{
				if ( cc0 == CLIP_CODE_L )
				{
					vv0 = 0; vv1 = 1; vv2 = 2;
				}
				else if ( cc1 == CLIP_CODE_L )
				{
					vv0 = 1; vv1 = 2; vv2 = 0;
				}
				else
				{
					vv0 = 2; vv1 = 1; vv2 = 0;
				}

				vert0 = &pr->vlist[vv0];
				vert1 = &pr->vlist[vv1];
				vert2 = &pr->vlist[vv2];

				// clip edge v0->v1
				u = vec3D_Sub( vert1->v, vert0->v );
				t0 = ( rl->cam->nearZ - vert0->z ) / u.z;
				x0 = vert0->x + u.x * t0;
				y0 = vert0->y + u.y * t0;

				// clip edge v0->v2
				v = vec3D_Sub( vert2->v, vert0->v );
				t1 = ( rl->cam->nearZ - vert0->z ) / v.z;
				x1 = vert0->x + v.x * t1;
				y1 = vert0->y + v.y * t1;

				vert0->x = x0;
				vert0->y = y0;
				vert0->z = rl->cam->nearZ;

				rl->numPolys++;
				pr2 = &rl->plist[rl->numPolys];
				memcpy( pr2, pr, sizeof(POLYR) );

				

				pr2->vlist[vv1].x = x0;
				pr2->vlist[vv1].y = y0;
				pr2->vlist[vv1].z = rl->cam->nearZ;

				pr2->vlist[vv2].x = x1;
				pr2->vlist[vv2].y = y1;
				pr2->vlist[vv2].z = rl->cam->nearZ;

			} // end if vertsIn == 2
		} // end if

		rl->numPolys++;
		// Camera to view space

	} // end for pidx
	

	return 1;
}

/************************************************************************/
/* renderlist_Render                                                    */
/************************************************************************/
INLINE void renderlist_Render( RENDERLIST *rl )
{
	int32	pidx;
	POLYR	*pr;
	int32	i, j;
	VERTEX	*v0, *v1, *v2;
	for ( pidx = 0; pidx < rl->numPolys; pidx++ )
	{
		pr = &rl->plist[pidx];

		for ( i = 0; i < 3; i++ )
		{
			v0 = &pr->vlist[i];
			v0->x = rl->cam->viewDist * v0->x / v0->z;
			v0->y = rl->cam->viewDist * v0->y * ( rl->cam->aspectRatio / v0->z );

			v0->x = rl->cam->alpha + rl->cam->alpha * v0->x;
			v0->y = rl->cam->beta - rl->cam->beta * v0->y;
		} // end for i

		v0 = &pr->vlist[0];
		v1 = &pr->vlist[1];
		v2 = &pr->vlist[2];

		if ( pr->attr & ATTR_TEXTURED )
		{
			draw_PolySolid( rl->vb, pr );
		}

		if ( pr->attr & ATTR_WIRED )
		{
			paint_Line( rl->vb, v0->x, v0->y, v1->x, v1->y, pr->color );
			paint_Line( rl->vb, v1->x, v1->y, v2->x, v2->y, pr->color );
			paint_Line( rl->vb, v2->x, v2->y, v0->x, v0->y, pr->color );				
		}
		

	} // end for pidx

}