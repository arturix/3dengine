#pragma once

#include "global.h"
#include "videobuff.h"
#include "camera.h"
#include "renderlist.h"

CAMERA g_Cam;
RENDERLIST	g_RL;

#include "object.h"
#include "objectcontainer.h"
#include <ctime>
#include <windows.h>

OBJECT obj;

/************************************************************************/
/* engine_Create                                                        */
/************************************************************************/
int32 engine_Create( real width, real height, pixel bgColor )
{
	VECTOR3D camPos = {0,0,-70};

	srand( time (NULL) );

	geom_BuildLookupTables();

	if ( !videobuff_Create( &g_VB, width, height, bgColor ) )
		return 0;

	if ( !camera_Create( &g_Cam, width, height, camPos, 50, 1000, 120 ) )
		return 0;

	if ( !renderlist_Create( &g_RL, &g_VB, &g_Cam ) )
		return 0;

	if ( !objectcontainer_Create() )
		return 0;

	object_CreateQuad( &obj, 2, 2, 25, 25 );

	objectcontainer_AddObject( &obj, STATE_ON, (ATTR_2SIDED|ATTR_WIRED|ATTR_TEXTURED), g_colorRed );
	objectcontainer_AddObject( &obj, STATE_ON, (ATTR_2SIDED|ATTR_WIRED|ATTR_TEXTURED), g_colorGreen );

	return 1;
}

/************************************************************************/
/* engine_Destroy                                                       */
/************************************************************************/
void engine_Destroy()
{
	objectcontainer_Destory();
	renderlist_Destroy( &g_RL );
	camera_Destroy( &g_Cam );
	videobuff_Destroy( &g_VB );
}

/************************************************************************/
/* engine_Main                                                          */
/************************************************************************/
INLINE void engine_Main()
{
	int32 i;
	OBJECTCONTAINER *oc;
	static real angle = 0;

	videobuff_Clear( &g_VB );
	camera_Euler( &g_Cam );
	renderlist_Reset( &g_RL );

	//angle = 45;
	objectcontainerList[0].m = matrix4x3_RotYY( angle );
	objectcontainerList[1].m = matrix4x3_RotYY( -angle );
	angle++;
	
	for ( i = 0; i < objectcontainerSize; i++ )
	{
		oc = &objectcontainerList[i];
		if ( oc->state & STATE_ON )
			renderlist_AddObject( &g_RL, oc );
	} // end for i

	renderlist_Render( &g_RL );
}

#define MOVE_STEP 1.0
/************************************************************************/
/* engine_KeyboardBehavior1                                             */
/************************************************************************/
INLINE void engine_KeyboardBehavior1( int32 key )
{
		
	if ( key == KEY_LEFT )
	{
		g_Cam.pos.x -= MOVE_STEP;
		return;
	}

	if ( key == KEY_RIGHT )
	{
		g_Cam.pos.x += MOVE_STEP;
		return;
	}
}

/************************************************************************/
/* engine_KeyboardBehavior2                                             */
/************************************************************************/
INLINE void engine_KeyboardBehavior2( int32 key )
{
	if ( key == KEY_UP )
	{
		g_Cam.pos.y += MOVE_STEP;
		return;
	}

	if ( key == KEY_DOWN )
	{
		g_Cam.pos.y -= MOVE_STEP;
		return;
	}
}

/************************************************************************/
/* engine_KeyboardBehavior3                                             */
/************************************************************************/
INLINE void engine_KeyboardBehavior3( int32 key )
{
	if ( key == KEY_UP )
	{
		g_Cam.pos.z += MOVE_STEP;
		return;
	}

	if ( key == KEY_DOWN )
	{
		g_Cam.pos.z -= MOVE_STEP;
		return;
	}
}

/************************************************************************/
/* engine_KeyboardBehavior4                                             */
/************************************************************************/
INLINE void engine_KeyboardBehavior4( int32 key )
{
}

/************************************************************************/
/* engine_Keyboard                                                      */
/************************************************************************/
INLINE void engine_Keyboard( int32 key )
{
	engine_KeyboardBehavior1( key );
	engine_KeyboardBehavior2( key );
	//engine_KeyboardBehavior3( key );

	if ( key == KEY_ESC )
	{
		PostQuitMessage( 0 );
	}
}


/************************************************************************/
/* engine_Mouse                                                         */
/************************************************************************/
INLINE void engine_Mouse()
{

}