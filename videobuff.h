#pragma once

#include "global.h"

/************************************************************************/
/* videobuff_Create                                                     */
/************************************************************************/
int32 videobuff_Create( VIDEOBUFF *vb, int32 width, int32 height, pixel bgColor )
{
	vb->width	= width;
	vb->height	= height;
	vb->size	= width*height;
	vb->minx	= 0;
	vb->miny	= 0;
	vb->maxx	= width-1;
	vb->maxy	= height-1;
	vb->bgColor	= bgColor;

	vb->buff	= (pixel*) malloc( vb->size * sizeof(pixel) );
	if ( !vb->buff )
		return 0;

	vb->zbuff	= (int32*) malloc( vb->size * sizeof(int32) );
	if ( !vb->zbuff )
		return 0;

	return 1;
}

/************************************************************************/
/* videobuff_Destroy                                                    */
/************************************************************************/
void videobuff_Destroy( VIDEOBUFF *vb )
{

}

/************************************************************************/
/* videobuff_Clear                                                      */
/************************************************************************/
INLINE void videobuff_Clear( VIDEOBUFF *vb )
{	
	memory_SetWord( vb->buff, vb->bgColor.p, vb->size );
	memory_SetDword( vb->zbuff, 0, vb->size );
}