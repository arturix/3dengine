#pragma once

#include <Windows.h>
#include <tchar.h>
#include "engine.h"

#define CLS_NAME	_T("My3DClass")
#define WND_NAME	_T("My 3D Window")

#define KEYDOWN(vk_code) ( ( GetAsyncKeyState( vk_code ) & 0x8000 ) ? (1) : (0) )

HBITMAP	hBitmap;
HDC		hdcBackBuffer;

/************************************************************************/
/* WndProc                                                              */
/************************************************************************/
LRESULT	_stdcall WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	PAINTSTRUCT ps;
	switch ( uMsg )
	{

	case WM_PAINT:
		BeginPaint( hWnd, &ps );
		BitBlt( ps.hdc, 0, 0, VIEWPORT_WIDTH, VIEWPORT_HEIGHT, hdcBackBuffer, 0, 0, SRCCOPY );
		EndPaint( hWnd, &ps );
		return 0;

	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;
	}
	return DefWindowProc( hWnd, uMsg, wParam, lParam );
}

/************************************************************************/
/* wnd_Create                                                           */
/************************************************************************/
int32 wnd_Create()
{
	WNDCLASSEX		wc;
	HINSTANCE		hInst;
	HWND			hWnd;
	MSG				msg;

	HDC				hdc;
	BITMAPINFO		bi;

	hInst			= GetModuleHandle( NULL );

	wc.cbSize			= sizeof(WNDCLASSEX);
	wc.style			= CS_HREDRAW|CS_VREDRAW;
	wc.cbClsExtra		= NULL;
	wc.cbWndExtra		= NULL;
	wc.lpfnWndProc		= WndProc;
	wc.hInstance		= hInst;
	wc.hIcon			= LoadIcon( NULL, IDI_APPLICATION );
	wc.hIconSm			= wc.hIcon;
	wc.hCursor			= LoadCursor( NULL, IDC_ARROW );
	wc.lpszClassName	= CLS_NAME;
	wc.lpszMenuName		= NULL;
	wc.hbrBackground	= NULL;

	if ( !RegisterClassEx( &wc ) )
		return 0;

	hWnd	= CreateWindowEx( NULL,
						      CLS_NAME, WND_NAME,
							  WS_OVERLAPPEDWINDOW, 
							  CW_USEDEFAULT, CW_USEDEFAULT,
							  800, 800,
							  NULL, NULL, hInst, NULL );

	if ( !hWnd )
		return 0;

	if ( !engine_Create( VIEWPORT_WIDTH, VIEWPORT_HEIGHT, g_colorBlack ) )
		return 0;


	{
		hdc	= CreateCompatibleDC( NULL );
		
		bi.bmiHeader.biSize			= sizeof(BITMAPINFO);
		bi.bmiHeader.biBitCount		= 16;
		bi.bmiHeader.biCompression	= BI_RGB;
		bi.bmiHeader.biWidth		= VIEWPORT_WIDTH;
		bi.bmiHeader.biHeight		= -VIEWPORT_HEIGHT;
		bi.bmiHeader.biPlanes		= 1;
		
		hBitmap = CreateDIBSection( hdc, &bi, DIB_RGB_COLORS, (void**) &g_VB.buff, NULL, 0 );
		if ( !hBitmap )
		{
			DeleteDC( hdc );
			return 0;
		}		

		hdcBackBuffer = CreateCompatibleDC( NULL );
		SelectObject( hdcBackBuffer, hBitmap );
		DeleteDC( hdc );
	} // end block

	

	ShowWindow( hWnd, SW_SHOW );
	UpdateWindow( hWnd );

	SetTimer( hWnd, NULL, 10, NULL );
	while ( GetMessage( &msg, NULL, 0, 0 ) )
	{
		
		// KEYBOARD
		if ( KEYDOWN( VK_ESCAPE ) )
			engine_Keyboard( KEY_ESC );
		else if ( KEYDOWN ( VK_UP ) )
			engine_Keyboard( KEY_UP );
		else if ( KEYDOWN ( VK_DOWN ) )
			engine_Keyboard( KEY_DOWN );
		else if ( KEYDOWN ( VK_LEFT ) )
			engine_Keyboard( KEY_LEFT );
		else if ( KEYDOWN ( VK_RIGHT ) )
			engine_Keyboard( KEY_RIGHT );

		engine_Main();

		InvalidateRect( hWnd, NULL, TRUE );
		UpdateWindow( hWnd );

		DispatchMessage( &msg );
		TranslateMessage( &msg );
	} // end while
	KillTimer( hWnd, NULL );

	UnregisterClass( CLS_NAME, hInst );

	engine_Destroy();
	
	return 1;
}