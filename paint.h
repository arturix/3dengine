#pragma once

#include "line.h"

/************************************************************************/
/* paint_Line                                                           */
/************************************************************************/
INLINE void paint_Line( VIDEOBUFF *vb, int32 x0, int32 y0, int32 x1, int32 y1, pixel color )
{
	if ( line_Clip( vb, &x0, &y0, &x1, &y1 ) )
		line_Bresham( vb, x0, y0, x1, y1, color );
}

/************************************************************************/
/* draw_PolySolid                                                       */
/************************************************************************/
INLINE void draw_PolySolid( VIDEOBUFF *vb, POLYR* p )
{
	int32 vv0 = 0, vv1 = 1, vv2 = 2;
	int32 x0, y0, z0;
	int32 x1, y1, z1;
	int32 x2, y2, z2;
	int32 dxdyl, dxdyr;
	int32 dzdyl, dzdyr;
	int32 dx, dy, dz;
	int32 xl, xr;
	int32 zl, zr;
	int32 ystart, yend;
	int32 xstart, xend;
	int32 *dxdy, *dzdy;
	int32 *x, *z;
	int32 xi, yi, zi;

	pixel *buff;
	int32 *zbuff;

	if ( p->vlist[vv1].y < p->vlist[vv0].y )
		SWAP_INT( vv0, vv1 );
	if ( p->vlist[vv2].y < p->vlist[vv0].y )
		SWAP_INT( vv0, vv2 );
	if ( p->vlist[vv2].y < p->vlist[vv1].y )
		SWAP_INT( vv1, vv2 );

#define FP_SHIFT	16

	x0 = ((int32)p->vlist[vv0].x) << FP_SHIFT;
	x1 = ((int32)p->vlist[vv1].x) << FP_SHIFT;
	x2 = ((int32)p->vlist[vv2].x) << FP_SHIFT;

	y0 = p->vlist[vv0].y;
	y1 = p->vlist[vv1].y;
	y2 = p->vlist[vv2].y;

#define _1_shift_left_28 (1<<28)

	z0 = _1_shift_left_28 / (int32)(p->vlist[vv0].z);
	z1 = _1_shift_left_28 / (int32)(p->vlist[vv1].z);
	z2 = _1_shift_left_28 / (int32)(p->vlist[vv2].z);

	// LHS
	dxdyl = x1 - x0;
	dzdyl = z1 - z0;
	if ( y1 != y0 )
	{
		dy = y1 - y0;
		dxdyl /= dy;
		dzdyl /= dy;
	}

	// RHS
	dxdyr = x2 - x0;
	dzdyr = z2 - z0;
	if ( y2 != y0 )
	{
		dy = y2 - y0;
		dxdyr /= dy;
		dzdyr /= dy;
	}

	if ( y0 < vb->miny )
	{
		dy = vb->miny - y0;
		xl = dxdyl * dy + x0;
		xr = dxdyr * dy + x0;
		zl = dzdyl * dy + z0;
		zr = dzdyr * dy + z0;
		ystart = vb->miny;
	}
	else
	{
		xl = xr = x0;
		zl = zr = z0;

		ystart = y0;
	}
	

	buff = vb->buff + ystart * vb->width;
	zbuff = vb->zbuff + ystart * vb->width;

	if ( y1 < vb->maxy )
		yend = y1;
	else
		yend = vb->maxy;

	if ( dxdyr < dxdyl )
	{
		SWAP_INT( dxdyl, dxdyr );
		SWAP_INT( dzdyl, dzdyr );

		SWAP_INT( xl, xr );
		SWAP_INT( zl, zr );

		dxdy = &dxdyr;
		dzdy = &dzdyr;

		x = &xr;
		z = &zr;
	}
	else
	{
		dxdy = &dxdyl;
		dzdy = &dzdyl;

		x = &xl;
		z = &zl;
	}

#include "poly_solid.h"

	if ( yend == vb->maxy )
		return;
	
	*dxdy = x1 - x2;
	*dzdy = z1 - z2;
	if ( y2 != y1 )
	{
		dy = y1 - y2;
		*dxdy /= dy;
		*dzdy /= dy;
	}

	if ( y1 < vb->miny )
	{
		dy = vb->miny - y1;
		*x = *dxdy * dy + x1;
		*z = *dzdy * dy + z1;
		ystart = vb->miny;
	}
	else
	{
		*x = x1;
		*z = z1;

		ystart = y1;
	}	

	if ( y2 < vb->maxy )
		yend = y2;
	else
		yend = vb->maxy;

	#include "poly_solid.h"
}