#pragma once

#define CLIP_CODE_N	0x0008
#define CLIP_CODE_S	0x0004
#define CLIP_CODE_E	0x0002
#define CLIP_CODE_W	0x0001

#define CLIP_CODE_NE (CLIP_CODE_N|CLIP_CODE_E)
#define CLIP_CODE_NW (CLIP_CODE_N|CLIP_CODE_W)
#define CLIP_CODE_SE (CLIP_CODE_S|CLIP_CODE_E)
#define CLIP_CODE_SW (CLIP_CODE_S|CLIP_CODE_W)

#include "global.h"

/************************************************************************/
/* line_Bresham                                                         */
/************************************************************************/
INLINE void line_Bresham( VIDEOBUFF *vb, int32 x0, int32 y0, int32 x1, int32 y1, pixel color )
{
	int32 dx, dy;
	int32 dx2, dy2;
	int32 i;
	int32 err;
	int32 xInc, yInc;

	pixel	*buff;

	buff = (pixel*) vb->buff + x0 + y0 * vb->width;

	dx = x1 - x0;
	dy = y1 - y0;

	if ( dx >= 0 )
		xInc = 1;
	else
	{
		xInc = -1;
		dx = -dx;
	}

	if ( dy >= 0 )
		yInc = vb->width;
	else
	{
		yInc = -vb->width;
		dy = -dy;
	}

	dx2 = dx << 1;
	dy2 = dy << 1;

	if ( dx > dy )
	{
		err = dy2 - dx;
		for ( i = 0; i < dx; i++ )
		{
			*buff = color;
			if ( err > 0 )
			{
				err -= dx2;
				buff += yInc;
			}
			err += dy2;
			buff += xInc;
		} // end for i
	} // end if
	else
	{
		err = dx2 - dy;
		for ( i = 0; i < dy; i++ )
		{
			*buff = color;
			if ( err > 0 )
			{
				err -= dy2;
				buff += xInc;
			}
			err += dx2;
			buff += yInc;
		}		
	} // end else

}

/************************************************************************/
/* line_GetClipCode                                                     */
/************************************************************************/
INLINE int32 line_GetClipCode( VIDEOBUFF *vb, int32 x, int32 y )
{
	int32 c = 0;

	if ( y < vb->miny )
		c = CLIP_CODE_N;
	else if ( y > vb->maxy )
		c = CLIP_CODE_S;

	if ( x < vb->minx )
		c |= CLIP_CODE_W;
	else if (  x > vb->maxx )
		c |= CLIP_CODE_E;

	return c;
}

/************************************************************************/
/* line_Trim                                                            */
/************************************************************************/
INLINE int32 line_Trim( VIDEOBUFF *vb, int32 *x, int32 *y, int32 cc, real xOverY, real yOverX )
{
	int32 xc, yc;

	

	switch ( cc )
	{
	case CLIP_CODE_N:
		yc = vb->miny;
		xc = *x + ( yc - *y ) * xOverY;
		break;

	case CLIP_CODE_S:
		yc = vb->maxy;
		xc = *x + ( yc - *y ) * xOverY;
		break;

	case CLIP_CODE_W:
		xc = vb->minx;
		yc = *y + ( xc  - *x ) * yOverX;
		break;

	case CLIP_CODE_E:
		xc = vb->maxx;
		yc = *y + ( xc - *x ) * yOverX;
		break;

	case CLIP_CODE_NW:
		yc = vb->miny;
		xc = *x + ( yc - *y ) * xOverY;
		if ( xc < vb->minx || xc > vb->maxx )
		{
			xc = vb->minx;
			yc = *y + ( xc - *x ) * yOverX;
		}
		break;

	case CLIP_CODE_NE:
		yc = vb->miny;
		xc = *x + ( yc - *y ) * xOverY;
		if ( xc < vb->minx || xc > vb->maxx )
		{
			xc = vb->maxx;
			yc = *y + ( xc - *x ) * yOverX;
		}
		break;

	case CLIP_CODE_SW:
		yc = vb->maxy;
		xc = *x + ( yc - *y ) * xOverY;
		if ( xc < vb->minx || xc > vb->maxx )
		{
			xc = vb->minx;
			yc = *y + ( xc - *x ) * yOverX;
		}
		break;

	case CLIP_CODE_SE:
		yc = vb->maxy;
		xc = *x + ( yc - *y ) * xOverY;
		if ( xc < vb->minx || xc > vb->maxx )
		{
			xc = vb->maxx;
			yc = *y + ( xc - *x ) * yOverX;
		}
		break;

	default:		
		return 1;
	}

	if ( xc < vb->minx || xc > vb->maxx )
		return  0;

	if ( yc < vb->miny || yc > vb->maxy )
		return 0;

	*x = xc;
	*y = yc;

	return 1;
}

/************************************************************************/
/* line_Clip                                                            */
/************************************************************************/
INLINE int32 line_Clip( VIDEOBUFF *vb, int32 *x0, int32 *y0, int32 *x1, int32 *y1 )
{
	int32 c0, c1;
	real xOverY, yOverX;
	c0 = line_GetClipCode( vb, *x0, *y0 );
	c1 = line_GetClipCode( vb, *x1, *y1 );

	if ( c0 & c1 )
		return 0;

	if ( c0 == 0 && c1 == 0 )
		return 1;

	xOverY = (real)(*x1 - *x0) / (real)(*y1 - *y0);
	yOverX = (real)(*y1 - *y0) / (real)(*x1 - *x0);
	
	if ( !line_Trim( vb, x0, y0, c0, xOverY, yOverX ) )
		return 0;

	if ( !line_Trim( vb, x1, y1, c1, xOverY, yOverX ) )
		return 0;

	return 1;
}