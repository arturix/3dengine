#pragma once

#define OBJECT_MAX_VERTICES	1024
#define OBJECT_MAX_POLYS	1024

#include "poly.h"

typedef struct  
{

	int32	numPolys;
	POLY	plist[OBJECT_MAX_POLYS];

	int32	numVerts;
	VERTEX	vlist[OBJECT_MAX_VERTICES];
	int32	touchVertices[OBJECT_MAX_VERTICES];

} OBJECT;



/************************************************************************/
/* object_PolyNormals                                                   */
/************************************************************************/
void object_PolyNormals( OBJECT *o )
{
	int32 pidx;
	POLY *p;
	VECTOR3D v0, v1, v2;
	VECTOR3D u, v, n;

	for ( pidx = 0; pidx < o->numPolys; pidx++ )
	{
		p = &o->plist[pidx];

		v0 = o->vlist[p->vidx[0]].v;
		v1 = o->vlist[p->vidx[1]].v;
		v2 = o->vlist[p->vidx[2]].v;

		u = vec3D_Sub( v1, v0 );
		v = vec3D_Sub( v2, v0 );
		n = vec3D_Cross( u, v );

		p->normal = n;
		p->nlength = vec3D_Len( n );
	} // end for pidx
}

/************************************************************************/
/* object_VertexNormals                                                 */
/************************************************************************/
void object_VertexNormals( OBJECT *o )
{
	int32 idx;
	POLY *p;
	VERTEX *v0, *v1, *v2;
	VECTOR3D u, v, n;

	for ( idx = 0; idx < o->numVerts; idx++ )
		o->touchVertices[idx] = 0;

	for ( idx = 0; idx < o->numPolys; idx++ )
	{
		p = &o->plist[idx];

		o->touchVertices[p->vidx[0]]++;
		o->touchVertices[p->vidx[1]]++;
		o->touchVertices[p->vidx[2]]++;

		v0 = &o->vlist[p->vidx[0]];
		v1 = &o->vlist[p->vidx[1]];
		v2 = &o->vlist[p->vidx[2]];

		u = vec3D_Sub( v1->v, v0->v );
		v = vec3D_Sub( v2->v, v0->v );
		n = vec3D_Cross( u, v );

		v0->normal = vec3D_Add( v0->normal, n );
		v1->normal = vec3D_Add( v1->normal, n );
		v2->normal = vec3D_Add( v2->normal, n );
	} // end for idx

	for ( idx = 0; idx < o->numVerts; idx++ )
	{
		if ( o->touchVertices[idx] == 0 )
			continue;

		 o->vlist[idx].normal = vec3D_DivScalar( o->vlist[idx].normal, o->touchVertices[idx] );
	}
}

/************************************************************************/
/* object_Create                                                        */
/************************************************************************/
int32 object_Create( OBJECT *o )
{

	return 1;
}


/************************************************************************/
/* object_CreateQuad                                                    */
/************************************************************************/
int32 object_CreateQuad( OBJECT *o, int32 rows, int32 cols, real width, real height )
{
	int32 idx;
	int32 poly;	
	int32 r, c;
	real vcol, vrow;
	real tcol, trow;
	VERTEX *v;
	POLY *p;

	vcol = width / ( cols-1 );
	vrow = height / ( rows-1 );

	o->numVerts = rows * cols;
	o->numPolys = (rows-1) * (cols-1) * 2;

	for ( r = 0; r < rows; r++ )
	{
		for ( c = 0; c < cols; c++ )
		{
			idx = r * cols + c;
			v = &o->vlist[idx];
			v->x = c * vcol - ( width / 2 );
			v->y = r * vrow - ( height / 2 );
			v->z = 0.0;
		} // end for c
	} // end for r

	for ( poly = 0; poly < o->numPolys/2; poly++ )
	{
		idx = (poly % (cols-1)) + cols * (poly / (cols-1));
		p = &o->plist[poly*2];
		
		p->vidx[0] = p->tidx[0] = idx;
		p->vidx[1] = p->tidx[1] = idx+cols;
		p->vidx[2] = p->tidx[2] = idx+cols+1;

		p = &o->plist[poly*2+1];
		p->vidx[0] = p->tidx[0] = idx;
		p->vidx[1] = p->tidx[1] = idx+cols+1;
		p->vidx[2] = p->tidx[2] = idx+1;
	}
	return 1;
}