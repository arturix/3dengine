#pragma once

#include "matrix.h"

typedef struct  
{
	VECTOR3D	pos, dir;
	MATRIX4x3	mcam;

	real nearZ, farZ;
	real fov;
	real aspectRatio;
	real viewportWidth;
	real viewportHeight;
	real viewDist;

	real zFactorX;
	real zFactorY;

	real alpha, beta;
} CAMERA;

/************************************************************************/
/* camera_Create                                                        */
/************************************************************************/
int32 camera_Create( CAMERA *cam, real width, real height, VECTOR3D pos, real nearZ, real farZ, real fov )
{
	cam->pos	= pos;
	cam->nearZ	= nearZ;
	cam->farZ	= farZ;
	cam->aspectRatio = width/height;
	cam->viewportWidth	= 2.0;
	cam->viewportHeight	= 2.0 / cam->aspectRatio;
	cam->viewDist	= cam->viewportWidth / 2.0 * tan( c_degToRad * (fov/2.0) );

	cam->zFactorX = cam->viewportWidth / 2.0 / cam->viewDist;
	cam->zFactorY = cam->viewportHeight / 2.0 / cam->viewDist;

	cam->alpha = width / 2;
	cam->beta = height / 2;

	return 1;
}

/************************************************************************/
/* camera_Destroy                                                       */
/************************************************************************/
void camera_Destroy( CAMERA *cam )
{

}

/************************************************************************/
/* camera_Euler                                                         */
/************************************************************************/
INLINE void camera_Euler( CAMERA *cam )
{
	real s, c;
	MATRIX4x3 mx, my, mz, mrot;
	MATRIX4x3 m = 
	{
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		-cam->pos.x, -cam->pos.y, -cam->pos.z
	};

	s = -geom_Sin( cam->dir.x );
	c = geom_Cos( cam->dir.x );
	mx = matrix4x3_RotX( s, c );

	s = -geom_Sin( cam->dir.y );
	c = geom_Cos( cam->dir.y );
	my = matrix4x3_RotY( s, c );

	s = -geom_Sin( cam->dir.z );
	c = geom_Cos( cam->dir.z );
	mz = matrix4x3_RotX( s, c );

	mrot = matrix4x3_Mul( mx, my );
	mrot = matrix4x3_Mul( mrot, mz );
	cam->mcam = matrix4x3_Mul( m, mrot );
}