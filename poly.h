#pragma once

#define POLY_ATTR_SHADE_MODE_CONST		0x0020
#define POLY_ATTR_SHADE_MODE_EMMISIVE	0x0020
#define POLY_ATTR_SHADE_MODE_FLAT		0x0040
#define POLY_ATTR_SHADE_MODE_GORAUD		0x0080
#define POLY_ATTR_SHADE_MODE_PHONG		0x0100
#define POLY_ATTR_SHADE_MODE_FAST_PHONG	0x0100
#define POLY_ATTR_SHADE_MODE_TEXTURED	0x0200

#include "vector.h"

typedef struct
{
	int32	state;
	int32	attr;
	pixel	color;

	real		nlength;
	VECTOR3D	normal;

	int32		vidx[3];
	int32		tidx[3];
} POLY;


typedef struct  
{
	int32	state;
	int32	attr;
	pixel	color;

	VERTEX	vlist[3];
} POLYR;
