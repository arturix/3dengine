for ( yi = ystart; yi < yend; yi++ )
{
	xstart = xl >> FP_SHIFT;
	xend = xr >> FP_SHIFT;

	zi = zl;

	if ( xstart != xend )
	{
		dx = ( xend - xstart );
		dz = ( zr - zl ) / dx;
	}
	else
	{
		dz = 0;
	}

	if ( xstart < vb->minx )
	{
		dx = vb->minx - xstart;
		zi = dx * dz + zi;
		xstart = vb->minx;
	}

	if ( xend > vb->maxx )
		xend = vb->maxx;

	for ( xi = xstart; xi < xend; xi++ )
	{
		if ( zi > zbuff[xi] )
		{
			buff[xi] = p->color;
			zbuff[xi] = zi;
		}
		zi += dz;
		
	} // end for xi

	xl += dxdyl;
	xr += dxdyr;

	zl += dzdyl;
	zr += dzdyr;

	buff += vb->width;
	zbuff += vb->width;
} // end for yi