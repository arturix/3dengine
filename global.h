#pragma once

#define INLINE __inline 

#define VIEWPORT_WIDTH	640
#define VIEWPORT_HEIGHT	480

#include <math.h>
#include <assert.h>

typedef unsigned char	uint8;
typedef char			int8;
typedef unsigned short	uint16;
typedef short			int16;
typedef unsigned long	uint32;
typedef long			int32;
typedef double			real;

typedef struct  
{
	union
	{
		unsigned short	p;
		struct  
		{
			unsigned short b : 5;
			unsigned short g : 5;
			unsigned short r : 5;
			unsigned short a : 1;
		};
	};
} pixel;

typedef struct  
{
	int32	width;
	int32	height;
	int32	size;
	int32	minx, maxx;
	int32	miny, maxy;
	pixel	bgColor;
	pixel	*buff;
	int32	*zbuff;
} VIDEOBUFF;

VIDEOBUFF	g_VB;

//////////////////////////////////////////////////////////////////////////
///

#define STATE_OFF	0x0000
#define STATE_ON	0x0001

#define ATTR_WIRED		0x0001
#define ATTR_2SIDED		0x0002
#define ATTR_TEXTURED	0x0004


//////////////////////////////////////////////////////////////////////////
/// UTILS
#define RAND_INT(x,y) ( rand() % ((y)-(x)+1)+(x) )
#define RAND_REAL(x,y) ( ((real) (rand())) / ((real)RAND_MAX) * ((y)-(x)) + (x) )

#define SWAP_INT( x, y ) { (x)^=(y); (y)^=(x); (x)^=(y); }

//////////////////////////////////////////////////////////////////////////
/// COLORS

#define RGB_COLOR(r,g,b) ( (b) + ((g&31)<<5) + ((r&31)<<10) )

const pixel	g_colorRed		= { RGB_COLOR(31,0,0) };
const pixel	g_colorGreen	= { RGB_COLOR(0,31,0) };
const pixel	g_colorBlue		= { RGB_COLOR(0,0,31) };
const pixel	g_colorWhite	= { RGB_COLOR(31,31,31) };
const pixel	g_colorBlack	= { RGB_COLOR(0,0,0) };

//////////////////////////////////////////////////////////////////////////
/// GEOMETRY
real c_sinLookupTable[361];
real c_cosLookupTable[361];

const real c_degToRad = 0.0174532925;
const real c_radToDeg = 57.2957795;

/************************************************************************/
/* geom_BuildLookupTables                                               */
/************************************************************************/
void geom_BuildLookupTables()
{
	int32 angle;
	real alpha;
	
	for ( angle = 0; angle < 361; angle++ )
	{
		alpha = c_degToRad * angle;
		c_sinLookupTable[angle] = sin( alpha );
		c_cosLookupTable[angle] = cos( alpha );
	} // end for 
}

/************************************************************************/
/* geom_Sin                                                             */
/************************************************************************/
INLINE real geom_Sin( real theta )
{
	int32 thetaInt;
	real thetaFrac;

	theta = fmodf( theta, 360.0 );
	if ( theta < 0 )
		theta += 360.0;

	thetaInt = theta;
	thetaFrac = theta - thetaInt;

	return c_sinLookupTable[thetaInt] + thetaFrac * ( c_sinLookupTable[thetaInt+1] - c_sinLookupTable[thetaInt] );
}

/************************************************************************/
/* geom_Cos                                                             */
/************************************************************************/
INLINE real geom_Cos( real theta )
{
	int32 thetaInt;
	real thetaFrac;
	theta = fmodf( theta, 360.0 );
	if ( theta < 0 )
		theta += 360.0;

	thetaInt = theta;
	thetaFrac = theta - thetaInt;

	return c_cosLookupTable[thetaInt] + thetaFrac * ( c_cosLookupTable[thetaInt+1] - c_cosLookupTable[thetaInt] );
}

//////////////////////////////////////////////////////////////////////////
/// MEMORY

/************************************************************************/
/* memory_SetWord                                                       */
/************************************************************************/
INLINE void memory_SetWord( void *dest, int16 data, int32 count )
{
	_asm
	{
		mov edi, dest
		mov ecx, count
		mov ax, data
		rep	stosw
	}
}

/************************************************************************/
/* memory_SetDword                                                      */
/************************************************************************/
INLINE void memory_SetDword( void *dest, int32 data, int32 count )
{
	_asm
	{
		mov edi, dest
	    mov ecx, count
		mov eax, data
		rep stosd
	}
}

//////////////////////////////////////////////////////////////////////////
/// KEYCODES
#define KEY_ESC		1
#define KEY_UP		2
#define KEY_DOWN	3
#define KEY_LEFT	4
#define KEY_RIGHT	5

