#pragma once

#include "vector.h"

typedef struct  
{	
	union
	{
		real m[12];
		struct  
		{
			real m11, m12, m13;
			real m21, m22, m23;
			real m31, m32, m33;
			real tx, ty, tz;
		};		
	};
} MATRIX4x3;

const MATRIX4x3 c_Matrix4x3Identity = 
{
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0,
	0.0, 0.0, 0.0
};

/************************************************************************/
/* matrix4x3_RotX                                                       */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_RotX( real s, real c )
{
	MATRIX4x3 m = 
	{
		1.0, 0.0, 0.0,
		0.0, c, -s,
		0.0, s, c,
		0.0, 0.0, 0.0
	};

	return m;
}

/************************************************************************/
/* matrix4x3_RotY                                                       */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_RotY( real s, real c )
{
	MATRIX4x3 m = 
	{
		c, 0.0, s,
		0.0, 1.0, 0.0,
		-s, 0.0, c,
		0.0, 0.0, 0.0
	};

	return m;
}

/************************************************************************/
/* matrix4x3_RotZ                                                       */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_RotZ( real s, real c )
{
	MATRIX4x3 m = 
	{
		c, -s, 0.0,
		s, c, 0.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 0.0
	};

	return m;
}

/************************************************************************/
/* matrix4x3_RotXX                                                      */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_RotXX( real theta )
{
	real s, c;
	s = geom_Sin( theta );
	c = geom_Cos( theta );
	return matrix4x3_RotX( s, c );
}

/************************************************************************/
/* matrix4x3_RotYY                                                      */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_RotYY( real theta )
{
	real s, c;
	s = geom_Sin( theta );
	c = geom_Cos( theta );
	return matrix4x3_RotY( s, c );
}

/************************************************************************/
/* matrix4x3_RotZZ                                                      */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_RotZZ( real theta )
{
	real s, c;
	s = geom_Sin( theta );
	c = geom_Cos( theta );
	return matrix4x3_RotZ( s, c );
}

/************************************************************************/
/* matrix4x3_Mul                                                        */
/************************************************************************/
INLINE MATRIX4x3 matrix4x3_Mul( MATRIX4x3 a, MATRIX4x3 b )
{
	MATRIX4x3 m = 
	{
		a.m11 * b.m11 + a.m12 * b.m21 + a.m13 * b.m31,
		a.m11 * b.m12 + a.m12 * b.m22 + a.m13 * b.m32,
		a.m11 * b.m13 + a.m12 * b.m23 + a.m13 * b.m33,

		a.m21 * b.m11 + a.m22 * b.m21 + a.m23 * b.m31,
		a.m21 * b.m12 + a.m22 * b.m22 + a.m23 * b.m32,
		a.m21 * b.m13 + a.m22 * b.m23 + a.m23 * b.m33,

		a.m31 * b.m11 + a.m32 * b.m21 + a.m33 * b.m31,
		a.m31 * b.m12 + a.m32 * b.m22 + a.m33 * b.m32,
		a.m31 * b.m13 + a.m32 * b.m23 + a.m33 * b.m33,

		a.tx * b.m11 + a.ty * b.m21 + a.tz * b.m31 + b.tx,
		a.tx * b.m12 + a.ty * b.m22 + a.tz * b.m32 + b.ty,
		a.tx * b.m13 + a.ty * b.m23 + a.tz * b.m33 + b.tz,
	};
	
	return m;
}

/************************************************************************/
/* matrix4x3_MulVec3D                                                   */
/************************************************************************/
INLINE VECTOR3D matrix4x3_MulVec3D( VECTOR3D v, MATRIX4x3 m )
{
	VECTOR3D r = 
	{
		v.x * m.m11 + v.y * m.m21  + v.z * m.m31 + m.tx,
		v.x * m.m12 + v.y * m.m22  + v.z * m.m32 + m.ty,
		v.x * m.m13 + v.y * m.m23  + v.z * m.m33 + m.tz
	};
	return r;
}