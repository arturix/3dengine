#pragma once

#include "matrix.h"
#include "object.h"
#include "twoquads.h"

typedef struct  
{
	int32		id;
	int32		state;
	int32		attr;
	pixel		color;

	real		maxRadius;
	real		avgRadius;

	VECTOR3D	pos;
	VECTOR3D	dir;
	MATRIX4x3	m;

	OBJECT	*o;
} OBJECTCONTAINER;

#define OBJECTCONTAINER_MAX_SIZE	20
int32 objectcontainerSize = 0;
OBJECTCONTAINER	objectcontainerList[OBJECTCONTAINER_MAX_SIZE];

/************************************************************************/
/* object_Radius                                                        */
/************************************************************************/
INLINE void object_Radius( OBJECTCONTAINER *oc )
{
	int32 i;
	real len;
	oc->maxRadius = -99999;
	oc->avgRadius = 0;
	for ( i = 0; i < oc->o->numVerts; i++ )
	{
		len = vec3D_Len( oc->o->vlist[i].v );
		if ( len > oc->maxRadius )
			oc->maxRadius = len;
		oc->avgRadius += len;
	}
	oc->avgRadius /= oc->o->numVerts;
}

/************************************************************************/
/* objectcontainer_Create                                               */
/************************************************************************/
int32 objectcontainer_Create()
{
	int32 i;
	objectcontainerSize = 0;

	for ( i = 0; i < OBJECTCONTAINER_MAX_SIZE; i++ )
	{
		objectcontainerList[i].id		= i;
		objectcontainerList[i].state	= STATE_OFF;
	}
	return 1;
}

/************************************************************************/
/* objectcontainer_Destory                                              */
/************************************************************************/
void objectcontainer_Destory()
{

}

/************************************************************************/
/* objectcontainer_AddObject                                            */
/************************************************************************/
int32 objectcontainer_AddObject( OBJECT *o, int32 state, int32 attr, pixel color )
{
	OBJECTCONTAINER *oc;

	assert( objectcontainerSize < OBJECTCONTAINER_MAX_SIZE );


	oc			= &objectcontainerList[objectcontainerSize];

	oc->o		= o;
	oc->state	= state;
	oc->attr	= attr;
	oc->color	= color;
	oc->m		= c_Matrix4x3Identity;

	object_Radius( oc );
	object_PolyNormals( o );
	object_VertexNormals( o );

	objectcontainerSize++;
	return (objectcontainerSize-1);
}
