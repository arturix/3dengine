#pragma once

#include "global.h"

typedef struct  
{
	real	x, y;
} VECTOR2D;

typedef struct  
{
	real	x, y, z;
} VECTOR3D;

typedef struct  
{
	union
	{
		real vertex[8];
		struct
		{
			real x, y, z;
			real nx, ny, nz;
			real tx, ty;
		};
		struct
		{
			VECTOR3D v;
			VECTOR3D normal;
			VECTOR2D t;
		};
	};
			
} VERTEX;

/************************************************************************/
/* vec3D_Add                                                            */
/************************************************************************/
INLINE VECTOR3D vec3D_Add( VECTOR3D a, VECTOR3D b )
{
	VECTOR3D v =
	{
		a.x + b.x,
		a.y + b.y,
		a.z + b.z
	};
	return v;
}

/************************************************************************/
/* vec3D_Sub                                                            */
/************************************************************************/
INLINE VECTOR3D vec3D_Sub( VECTOR3D a, VECTOR3D b )
{
	VECTOR3D v =
	{
		a.x - b.x,
		a.y - b.y,
		a.z - b.z
	};
	return v;
}

/************************************************************************/
/* vec3D_DivScalar                                                      */
/************************************************************************/
INLINE VECTOR3D vec3D_DivScalar( VECTOR3D v, real s )
{
	v.x /= s;
	v.y /= s;
	v.z /= s;
	return v;
}

/************************************************************************/
/* vector3D_Len                                                         */
/************************************************************************/
INLINE real vec3D_LenSqr( VECTOR3D v )
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

/************************************************************************/
/* vector3D_Len                                                         */
/************************************************************************/
INLINE real vec3D_Len( VECTOR3D v )
{
	return sqrt( vec3D_LenSqr( v ) );
}

/************************************************************************/
/* vec3D_Normal                                                         */
/************************************************************************/
INLINE VECTOR3D vec3D_Normal( VECTOR3D v )
{
	real t = 1.0 / vec3D_Len( v );
	VECTOR3D n = 
	{
		v.x * t,
		v.y * t,
		v.z * t
	};

	return n;
}

/************************************************************************/
/* vec3D_Dot                                                            */
/************************************************************************/
INLINE real vec3D_Dot( VECTOR3D a, VECTOR3D b )
{
	return a.x * b.x + a.y * b.y + a.z * b.z; 
}

/************************************************************************/
/* vec3D_Cross                                                          */
/************************************************************************/
INLINE VECTOR3D vec3D_Cross( VECTOR3D a, VECTOR3D b )
{
	VECTOR3D c = 
	{
		a.y * b.z - a.z * b.y,
		a.x * b.z - a.z * b.x,
		a.x * b.y - a.y * b.x
	};
	return c;
}